export const add = (title) => {
    return {
        type: "ADD",
        payload: title
    }
}

export const del = (id) => {
    return {
        type: "DEL",
        payload: id
    }
}

export const edit = (id, title) => {
    const newData = {
        id: id,
        title: title
    }
    return {
        type: "EDIT",
        payload: newData
    }
}

