const initialState = {
    todos: [
        {
            id: 1,
            title: "makan"
        }, {
            id: 2,
            title: "mandi"
        }
    ]
}

const listTask = (state = initialState, action) => {
    const {type, payload} = action
    switch(type) {
        case "ADD":
            const newItem = {
                id: state.todos.length + 1,
                title: payload
            }
            return {
                ...state,
                todos: [...state.todos, newItem]
            }
        case "DEL":
            return {
                ...state,
                todos: state.todos.filter(item => item.id !== payload)
            }
        case "EDIT": 
            let foundIndex = state.todos.findIndex(element => element.id === payload.id)
            state.todos.splice(foundIndex, 1, payload)
            break;
        default:
            return initialState
    }
}

export default listTask