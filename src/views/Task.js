import logo from './logo.svg'
import './App.css'
import React, { useState } from 'react'
import FormInput from '../components/FromInput'
import TodoItem from '../components/TodoItem'
import EditModal from '../components/EditModal'
import DeleteModal from '../components/DeleteModal'
import { useDispatch, useSelector } from 'react-redux'
import { del, edit, add } from './store/actions/listTask'

const Task = () => {
    const todos = useSelector(state => state.listTask.todos)
    const dispatch = useDispatch()

    const [isEdit, setIsEdit] = useState(false)
    const [isDelete, setIsDelete] = useState(false)
    const [idDelete, setIdDelete] = useState("")
    const [editData, setEditData] = useState({
        id: "",
        title: ""
    })

    const addTask = (param) => {
        dispatch(add(param))
    }

    const deleteTask = (id) => {
        dispatch(del(id))
        setIdDelete("")
        setIsDelete(false)
    }

    const openModal = (id, data) => {
        setIsEdit(true)
        setEditData({
            id: id,
            title: data
        })
    }

    const closeModal = () => { setIsEdit(false) }

    const setTitle = e => {
        setEditData({
            ...editData,
            title: e.target.value
        })
    }

    const updateData = () => {
        const { id, title } = editData
        dispatch(edit(id, title))
        setIsEdit(false)
        setEditData({
            id: "",
            title: ""
        })
    }

    const openDelete = (id) => {
        setIdDelete(id)
        setIsDelete(true)
    }

    const closeDelete = () => { setIsDelete(false) }

    return (
        <div className="app">
            <div className="logo">
                <img src={logo} alt="logo" />
                <h3>Task List</h3>
            </div>
            <div className="list">
                {todos.map(item =>
                    <TodoItem
                        key={item.id}
                        todo={item}
                        del={openDelete}
                        open={openModal}
                    />
                )}
            </div>
            <div className="form-input">
                <FormInput add={addTask}/>
            </div>
            <EditModal
                edit={isEdit}
                close={closeModal}
                change={setTitle}
                data={editData}
                save={updateData}
            />
            <DeleteModal
                del={isDelete}
                close={closeDelete}
                id={idDelete}
                yes={deleteTask}
            />
        </div>
    )
}

export default Task
