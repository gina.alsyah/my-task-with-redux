import logo from './logo.svg';
import './App.css';
import React from 'react';
import FormInput from './components/FromInput';
import TodoItem from './components/TodoItem';
import EditModal from './components/EditModal';
import DeleteModal from './components/DeleteModal';

class App extends React.Component {
    state = {
        todos: [
            {
                id: 1,
                title: "makan"
            }, {
                id: 2,
                title: "mandi"
            }
        ],
        isEdit: false,
        editData: {
            id: "",
            title: ""
        },
        isDelete: false,
        idDelete: ""
    }

    deleteTask = id => {
        this.setState({
            todos: this.state.todos.filter(item => item.id !== id),
            idDelete: "",
            isDelete: false
        })
    }

    addTask = (param) => {
        console.log(param)
        this.setState({
            todos: [...this.state.todos, {
                id: this.state.todos.length + 1,
                title: param
            }]
        })
    }

    openModal = (id, data) => {
        this.setState({
            isEdit: true,
            editData: {
                id: id,
                title: data
            }
        })
    }

    closeModal = () => {
        this.setState({
            isEdit: false
        })
    }

    setTitle = e => {
        this.setState({
            editData: {
                ...this.state.editData,
                title: e.target.value
            }
        })
    }

    updateData = () => {
        const { id, title } = this.state.editData
        const newData = {
            id: id,
            title: title
        }
        const newTodos = this.state.todos
        newTodos.splice(id - 1, 1, newData)
        this.setState({
            todos: newTodos,
            isEdit: false,
            editData: {
                id: "",
                title: ""
            },

        })
    }

    openDelete = (id) => {
        this.setState({
            isDelete: true,
            idDelete: id
        })
    }

    closeDelete = () => {
        this.setState({
            isDelete: false
        })
    }

    render() {
        const { todos } = this.state;
        return (
            <div className="app">
                <div className="logo">
                    <img src={logo} alt="logo" />
                    <h3>Task List</h3>
                </div>
                <div className="list">
                    {todos.map(item =>
                        <TodoItem
                            key={item.id}
                            todo={item}
                            del={this.openDelete}
                            open={this.openModal}
                        />
                    )}
                </div>
                <div className="form-input">
                    <FormInput add={this.addTask} />
                </div>
                <EditModal
                    edit={this.state.isEdit}
                    close={this.closeModal}
                    change={this.setTitle}
                    data={this.state.editData}
                    save={this.updateData}
                />
                <DeleteModal
                    del={this.state.isDelete}
                    close={this.closeDelete}
                    id={this.state.idDelete}
                    yes={this.deleteTask}
                />
            </div>
        );
    }
}

export default App;
