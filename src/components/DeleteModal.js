import React from "react";
import Button from "./Button";
import "../styles/EditModal.css"

class DeleteModal extends React.Component {
    render() {
        const { del, close, id, yes } = this.props
        if (del) {
            return (
                <div className="modal-container">
                    <div className="modal-box">
                        <h3>Are you sure?</h3>
                        <div className="btn-group">
                            <Button text="yes" variant="success" action={() => yes(id)}/>
                            <Button text="no" variant="warning" action={() => close()}/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return null;
        }
        
    }
    
}

export default DeleteModal;
